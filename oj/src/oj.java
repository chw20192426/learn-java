import java.util.Scanner;
//CP1092 1095 1088

//CP1099
public class oj {
    public static void main(String[] args) {
        Scanner a = new Scanner(System.in);
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int m = sc.nextInt();
        int[][] arr = new int[n][n];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                arr[i][j] = sc.nextInt();
            }
        }
        if (m == 0) {
            for (int i = 0; i < n; i++) {
                for (int j = 0; j < n; j++) {
                    if (i == j)
                        System.out.print(1 + " ");
                    else
                        System.out.print(0 + " ");
                }
                System.out.println();
            }
            System.exit(0);
        }
        int[][] arr1 = arr;
        for (int i = 1; i < m; i++) {
            arr1 = multiply(arr, arr1);
        }
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                System.out.print(arr1[i][j] + " ");
            }
            System.out.println();
        }
    }

    private static int[][] multiply(int[][] a, int[][] b) {
        int len = a.length;
        int[][] res = new int[len][len];
        for (int i = 0; i < len; i++) {
            for (int j = 0; j < len; j++) {
                for (int k = 0; k < len; k++) {
                    res[i][j] += a[i][k] * b[k][j];
                }
            }
        }
        return res;
    }
}
////CP1098 https://blog.csdn.net/weixin_42859280/article/details/85940385
//public class oj {
//    public static void main(String[] args) {
//        Scanner a = new Scanner(System.in);
//        double x1, y1, x2, y2, x3, y3, x4, y4, m1, n1, m2, n2;
//        double result = 0;
//        x1 = a.nextDouble();//x1<x2
//        y1 = a.nextDouble();//y1<y2
//        x2 = a.nextDouble();
//        y2 = a.nextDouble();
//        x3 = a.nextDouble();//x3<x4
//        y3 = a.nextDouble();//y3<y4
//        x4 = a.nextDouble();
//        y4 = a.nextDouble();
//        m1 = Math.max(Math.min(x1, x2), Math.min(x3, x4));
//        n1 = Math.max(Math.min(y1, y2), Math.min(y3, y4));
//        m2 = Math.min(Math.max(x1, x2), Math.max(x3, x4));
//        n2 = Math.min(Math.max(y1, y2), Math.max(y3, y4));
//        if (m1 < m2 && n1 < n2) {
//            result = (m2 - m1) * (n2 - n1);
//        } else {
//            result = 0.0;
//        }
//        System.out.printf("%.2f",result);
//    }
//}
//自己思考结果：
//        if (x1 < x2 && y1 > y2) {
//            double tmp = y1;
//            y1 = y2;
//            y2 = tmp;
//        }
//        if (x3 < x4 && y3 > y4) {
//            double tmp = y3;
//            y3 = y4;
//            y4 = tmp;
//        }
//
//        if (y3 >= y2 || y4 <= y1 || x3 >= x2 || x4 <= x1) {
//            result = 0.0;
//        } else if (y3 < y2 && y3 > y1 && x3 < x2 && x3 > x1) {
//            if (y4 > y2 && x4 > x2) {
//                result = (x2 - x3) * (y2 - y3);
//            } else if (y4 > y2) {
//                result = (x4 - x3) * (y2 - y3);
//            } else if (x4 > x2) {
//                result = (x2 - x3) * (y4 - y3);
//            } else {
//                result = (x4 - x3) * (y4 - y3);
//            }
//        } else if (x3 < x1 && y3 > y1) {
//            if (y4 < y2 && y4 > y1 && x4 < x2 && x4 > x1) {
//                result = (x1 - x3) * (y4 - y3);
//            } else {
//                result = (x2 - x1) * (y4 - y3);
//            }
//        } else if (x3 > x1 && y3 < y1) {
//            if (y4 < y2 && y4 > y1 && x4 < x2 && x4 > x1) {
//                result = (x4 - x3) * (y4 - y1);
//            } else {
//                result = (x2 - x3) * (y4 - y1);
//            }
//        } else {
//            if (y4 < y2 && y4 > y1 && x4 < x2 && x4 > x1) {
//                result = (x4 - x1) * (y4 - y1);
//            } else {
//                result = (x2 - x1) * (y4 - y1);
//            }
//        }
//        System.out.printf("%.2lf",result);
//                }
//                }
//CP1100
//public class oj {
//    public static void main(String[] args) {
//        Scanner a = new Scanner(System.in);
//        int n;
//        n = a.nextInt();
//        System.out.printf("%d:%d:%d", n / 3600, (n - 3600 * (n / 3600)) / 60, n % 60);
//    }
//}
//CP1061
//public class oj {
//    public static void main(String[] args) {
//        Scanner a = new Scanner(System.in);
//        String b = "";
//        b = a.nextLine();
//        String []s = new String[1000];
//        for (int i = 0; i < b.length(); i++) {
//            s[i] = b.substring(i, i + 1);
//        }
//        int x, y, loc;
//        x = y = loc = 0;
//        for (int i = 0;s[i]!=null;i++){
//            if (s[i].equals("P")){
//                switch (loc){
//                    case 0:
//                        y+=1;
//                        break;
//                    case 1:
//                        x-=1;
//                        break;
//                    case 2:
//                        y-=1;
//                        break;
//                    case 3:
//                        x+=1;
//                        break;
//                }
//            }
//            else if(s[i].equals("L")){
//                loc = (loc+1)%4;
//            }
//            else if(s[i].equals("R")) {
//                loc = loc - 1;
//                if (loc<0) loc+=4;
//            }
//        }
//        System.out.printf("%d %d\n",x,y);
//    }
//}
//CP1024
//public class oj {
//    public static void main(String[] args) {
//        Scanner a = new Scanner(System.in);
//        String b = "";
//        b = a.nextLine();
//        String[] s = new String[80];
//        for (int i = 0; i < b.length(); i++) {
//            s[i] = b.substring(i, i + 1);
//        }
//        String s1 = "";
//        for (int i = 0; s[i + 1] != null; i++) {
//            if (!s[i].matches("[a-zA-Z]+")) {   //第i个不为字母
//                if (s[i + 1].matches("[a-zA-Z]+")) {
//                    s1 += s[i + 1];
//                }
//            } else {                                  //第i个为字母
//                if (!s[i + 1].matches("[a-zA-Z]+")) {
//                    if (i == 0) {
//                        s1 += s[i];
//                    }
//                    s1 += "\n";
//                } else {
//                    if (i == 0) {
//                        s1 += s[i];
//                    }
//                    s1 += s[i + 1];
//                }
//            }
//        }
//        System.out.println(s1);
//    }
//}
//CP1023
//public class oj {
//    public static void main(String[] args) {
//        Scanner a = new Scanner(System.in);
//        String b = "";
//        b = a.nextLine();
//        String[] s = new String[80];
//        for (int i = 0; i < b.length(); i++) {
//            s[i] = b.substring(i, i + 1);
//        }
//        String s1 = s[0].toUpperCase();
//        for (int i = 1; s[i] != null; i++) {
//            if (!s[i - 1].matches("[a-zA-Z]+")) {
//                if (s[i].matches("[a-zA-Z]+")) {
//                    s1 += s[i].toUpperCase();
//                }
//                else {
//                    s1 += s[i];
//                }
//            } else {
//                s1 += s[i];
//            }
//
//        }
//        System.out.printf("%s\n", s1);
//        a.close();
//    }
//}
