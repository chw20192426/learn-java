import java.util.Scanner;

public class if_else {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        boolean condition = sc.nextBoolean();
        if (condition) {
            System.out.println("condition的值为真");
        } else {
            System.out.println("condition的值为假");
        }
    }
}
