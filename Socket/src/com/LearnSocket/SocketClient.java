package com.LearnSocket;

import static com.LearnSocket.SocketServer.SERVER_PORT;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class SocketClient {
    public static void main(String[] args) throws IOException {
        commWithServer();
    }

    private static void commWithServer() throws IOException {
        System.out.println("Server端启动，在端口" + SERVER_PORT + "监听……");
        try (
                Socket socket = new Socket("localhost", SERVER_PORT);
        ) {
            Chat chat = new Chat("服务端", null, socket);
            chat.chatting();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        System.out.println("连接断开");
    }
}
