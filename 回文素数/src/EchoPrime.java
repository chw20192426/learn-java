import java.util.Scanner;

public class EchoPrime {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int n = input.nextInt();
        for (int i = 1; i <= n; i++) {
            if (isprime(i) && isEchoNum(i)) {
                System.out.printf("%d\n", i);
            }
        }
    }

    static boolean isprime(int n) {
        int i;
        for (i = 2; i * i <= n; i++) {
            if (n % i == 0) {
                return false;
            }
        }
        return n > 1;
    }

    static boolean isEchoNum(int n) {
        int tmp=0;
        for(int i=n;i!=0;i/=10)
            tmp=tmp*10+i%10;
        return tmp==n;
    }
}